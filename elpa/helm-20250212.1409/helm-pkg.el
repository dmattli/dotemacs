;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "helm" "20250212.1409"
  "Helm is an Emacs incremental and narrowing framework."
  '((helm-core "4.0.1")
    (wfnames   "1.2"))
  :url "https://emacs-helm.github.io/helm/"
  :commit "98c0d4e62ee3690da97323a4215bddda7f51e4fa"
  :revdesc "98c0d4e62ee3"
  :keywords '("helm" "convenience" "files" "buffers" "grep" "completion" "lisp" "matching" "tools" "help")
  :authors '(("Thierry Volpiatto" . "thievol@posteo.net"))
  :maintainers '(("Thierry Volpiatto" . "thievol@posteo.net")))
