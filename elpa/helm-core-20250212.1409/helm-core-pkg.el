;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "helm-core" "20250212.1409"
  "Development files for Helm."
  '((emacs "25.1")
    (async "1.9.9"))
  :url "https://emacs-helm.github.io/helm/"
  :commit "98c0d4e62ee3690da97323a4215bddda7f51e4fa"
  :revdesc "98c0d4e62ee3"
  :authors '(("Thierry Volpiatto" . "thievol@posteo.net"))
  :maintainers '(("Thierry Volpiatto" . "thievol@posteo.net")))
