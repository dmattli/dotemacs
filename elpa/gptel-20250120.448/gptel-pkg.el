;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "gptel" "20250120.448"
  "Interact with ChatGPT or other LLMs."
  '((emacs     "27.1")
    (transient "0.4.0")
    (compat    "29.1.4.1"))
  :url "https://github.com/karthink/gptel"
  :commit "455f540e43c90e8c16cba463916235a22f8af971"
  :revdesc "455f540e43c9"
  :keywords '("convenience")
  :authors '(("Karthik Chikmagalur" . "karthik.chikmagalur@gmail.com"))
  :maintainers '(("Karthik Chikmagalur" . "karthik.chikmagalur@gmail.com")))
